﻿using UnityEngine;
using System.Collections;

public class tileMovement : MonoBehaviour {

	public float tilePosX;
	public float tilePosY;
	public float tilePosZ;

	void Start() {
		tilePosX = transform.position.x;
		tilePosY = transform.position.y;
		tilePosZ = transform.position.z;
	}
	void OnCollisionExit (Collision col) {
		//print("No longer in contact with " + col.transform.name);

		if (col.transform.tag == "Player") {
			GetComponent<Rigidbody>().isKinematic = false;
		}
	}
	void OnCollisionEnter (Collision chkFinish) {
		if (this.transform.tag == "Finish") {
			if (chkFinish.transform.tag == "Player") {
				Invoke ("DetermineIfWin", 1);
			}
		}
	}
	void DetermineIfWin () {
		if (this.transform.childCount > 0) {
			print ("YOU LOSE");
		} else {
			print("YOU ARE A WINNER!!!!!");
		}
	}
}
