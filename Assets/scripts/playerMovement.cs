﻿using UnityEngine;
using System.Collections;

public class playerMovement : MonoBehaviour {

	public float minSwipeDistY;
	public float minSwipeDistX;
	//public float minSwipeDist;
	//public float maxSwipeTime; 
	//public float swipeStartTime;

	//private bool couldBeSwipe;
	private float playerPositionX;
	private float playerPositionY;
	private float playerPositionZ;
	private Vector2 startPos;
	private bool touching = true;
	// Use this for initialization
	void Start () {
		playerPositionX = transform.position.x;
		playerPositionY = transform.position.y;
		playerPositionZ = transform.position.z;
	}
	
	// Update is called once per frame
	void Update () {
		if (!touching) {
			transform.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX;
			transform.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ;
			transform.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
		} else {
			if (Input.GetKeyDown (KeyCode.LeftArrow) || Input.GetKeyDown (KeyCode.A)) {
				transform.position = new Vector3 (playerPositionX - 1, playerPositionY, playerPositionZ);
				playerPositionX = transform.position.x;
			}
			if (Input.GetKeyDown (KeyCode.RightArrow) || Input.GetKeyDown (KeyCode.D)) {
				transform.position = new Vector3 (playerPositionX + 1, playerPositionY, playerPositionZ);
				playerPositionX = transform.position.x;
			}
		
			if (Input.GetKeyDown (KeyCode.UpArrow) || Input.GetKeyDown (KeyCode.W)) {
				transform.position = new Vector3 (playerPositionX, playerPositionY, playerPositionZ + 1);
				playerPositionZ = transform.position.z;
			}
			if (Input.GetKeyDown (KeyCode.DownArrow) || Input.GetKeyDown (KeyCode.S)) {
				transform.position = new Vector3 (playerPositionX, playerPositionY, playerPositionZ - 1);
				playerPositionZ = transform.position.z;
			}

			//////////////////////////////
			//////////////////////////////

			//#if UNITY_ANDROID
			if (Input.touchCount > 0) 
			{
				Touch touch = Input.touches[0];
				switch (touch.phase)
				{
				case TouchPhase.Began:
					startPos = touch.position;
					break;
				case TouchPhase.Ended:
					float swipeDistVertical = (new Vector3(0, touch.position.y, 0) - new Vector3(0, startPos.y, 0)).magnitude;
					if (swipeDistVertical > minSwipeDistY)
					{
						float swipeValue = Mathf.Sign(touch.position.y - startPos.y);
						if (swipeValue > 0)//up swipe
						{
							transform.position = new Vector3 (playerPositionX, playerPositionY, playerPositionZ + 1);
							playerPositionZ = transform.position.z;
							Debug.Log("Up Swipe");
						}
							else if (swipeValue < 0)//down swipe
						{
							transform.position = new Vector3 (playerPositionX, playerPositionY, playerPositionZ - 1);
							playerPositionZ = transform.position.z;
							Debug.Log("Down Swipe");
						}
					}
					float swipeDistHorizontal = (new Vector3(touch.position.x,0, 0) - new Vector3(startPos.x, 0, 0)).magnitude;
					if (swipeDistHorizontal > minSwipeDistX)
					{
						float swipeValue = Mathf.Sign(touch.position.x - startPos.x);
						if (swipeValue > 0)//right swipe
						{
							transform.position = new Vector3 (playerPositionX + 1, playerPositionY, playerPositionZ);
							playerPositionX = transform.position.x;
							Debug.Log("Right Swipe");
						}
							else if (swipeValue < 0)//left swipe
						{
							transform.position = new Vector3 (playerPositionX - 1, playerPositionY, playerPositionZ);
							playerPositionX = transform.position.x;
							Debug.Log("Left Swipe");
						}
					}
					break;
				}
			}

			//////////////////////////////
			//////////////////////////////


			
			//checkHorizontalSwipes ();

				
				playerPositionY = transform.position.y;
			}
			
			touching = false;
		}
		
		//void OnCollisionEnter (Collision collision) {
		//if (collision.gameObject.tag == "Finish") {
		//print ("YOU WIN!!!");
		//}
		//}
		void OnCollisionStay (Collision col) {
			touching = true;
			
			if (col.gameObject.tag == "Finish") {
				touching = false;
			}
		}

	//////////////////////////////
	//////////////////////////////
	
		//IEnumerator checkHorizontalSwipes () //Coroutine, which gets Started in "Start()" and runs over the whole game to check for swipes
	//{
		//while (true) { //Loop. Otherwise we wouldnt check continoulsy ;-)
			//foreach (Touch touch in Input.touches) { //For every touch in the Input.touches - array...
				
				//switch (touch.phase) {
				//case TouchPhase.Began: //The finger first touched the screen --> It could be(come) a swipe
					//couldBeSwipe = true;
					
					//startPos = touch.position;  //Position where the touch started
					//swipeStartTime = Time.time; //The time it started
					//break;
					
				//case TouchPhase.Stationary: //Is the touch stationary? --> No swipe then!
					//couldBeSwipe = false;
					//break;
				//}
				
				//float swipeTime = Time.time - swipeStartTime; //Time the touch stayed at the screen till now.
				//float swipeDist = Mathf.Abs (touch.position.x - startPos.x); //Swipe distance
				
				
				//if (couldBeSwipe && swipeTime < maxSwipeTime && swipeDist > minSwipeDist) {
					// It's a swiiiiiiiiiiiipe!
					//couldBeSwipe = false; //<-- Otherwise this part would be called over and over again.
					
					//if (Mathf.Sign (touch.position.x - startPos.x) == 1f) { //Swipe-direction, either 1 or -1.
						
						//Right-swipe
						//transform.position = new Vector3 (playerPositionX + 1, playerPositionY, playerPositionZ);
						//playerPositionX = transform.position.x;
						//Debug.Log ("Right Swipe");
						
					//} else {
						
						//Left-swipe
						//transform.position = new Vector3 (playerPositionX - 1, playerPositionY, playerPositionZ);
						//playerPositionX = transform.position.x;
						//Debug.Log ("Left Swipe");
					//}
				//} 
			//}
			//yield return null;
		//}
		
		//////////////////////////////
		//////////////////////////////
	}
	