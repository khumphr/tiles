﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class death : MonoBehaviour {
    
void OnCollisionEnter (Collision collision) {
		if (collision.gameObject.tag == "Player") {
			print ("you died");
			//Application.LoadLevel ("test1");
            SceneManager.LoadScene("test1");
		}
		if (collision.gameObject.tag != "Player") {
			Destroy(collision.gameObject);
		}
	}
}
